# Copyright (C) 2017 The Pure Nexus Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#  Don't build debug versions of ART products
PRODUCT_ART_TARGET_INCLUDE_DEBUG_BUILD = false

# Include overlays
PRODUCT_PACKAGE_OVERLAYS += \
    vendor/cylon/overlay/common

# Custom Packages
PRODUCT_PACKAGES += \
    libbfqio \
    MatLog \
    Terminal

# ART changes
PRODUCT_ART_TARGET_INCLUDE_DEBUG_BUILD = false
export ART_BUILD_TARGET_NDEBUG := true
export ART_BUILD_TARGET_DEBUG := false
export ART_BUILD_HOST_NDEBUG := true
export ART_BUILD_HOST_DEBUG := false

# Disable cfi sanitizer
ENABLE_CFI := false

# SystemUITests
EXCLUDE_SYSTEMUI_TESTS := true

# Backup Tool
ifeq ($(AB_OTA_UPDATER),true)
PRODUCT_COPY_FILES += \
    vendor/cylon/prebuilt/bin/backuptool_ab.sh:system/bin/backuptool_ab.sh \
    vendor/cylon/prebuilt/bin/backuptool_ab.functions:system/bin/backuptool_ab.functions \
    vendor/cylon/prebuilt/bin/backuptool_postinstall.sh:system/bin/backuptool_postinstall.sh
else
PRODUCT_COPY_FILES += \
    vendor/cylon/prebuilt/bin/backuptool.sh:install/bin/backuptool.sh \
    vendor/cylon/prebuilt/bin/backuptool.functions:install/bin/backuptool.functions
endif

# init.d support
PRODUCT_COPY_FILES += \
    vendor/cylon/prebuilt/etc/init.d/00banner:system/etc/init.d/00banner \
    vendor/cylon/prebuilt/bin/sysinit:system/bin/sysinit

# userinit support
PRODUCT_COPY_FILES += \
    vendor/cylon/prebuilt/etc/init.d/90userinit:system/etc/init.d/90userinit

# Cylon init
PRODUCT_COPY_FILES += \
    vendor/cylon/prebuilt/etc/init.cylon.rc:root/init.cylon.rc

# include definitions for SDCLANG
include vendor/cylon/sdclang/sdclang.mk
